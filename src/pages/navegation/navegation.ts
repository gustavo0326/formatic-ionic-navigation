import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ContadorPage } from '../contador/contador';
import { ProfilePage } from '../profile/profile';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the NavegationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-navegation',
  templateUrl: 'navegation.html',
})
export class NavegationPage {

  next: any = ContadorPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NavegationPage');
  }

  goTo(){
    this.navCtrl.parent.select(4);
  }

}
