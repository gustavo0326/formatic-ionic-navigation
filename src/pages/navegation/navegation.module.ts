import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavegationPage } from './navegation';

@NgModule({
  declarations: [
    NavegationPage,
  ],
  imports: [
    IonicPageModule.forChild(NavegationPage),
  ],
})
export class NavegationPageModule {}
